import { Component, HostListener, Inject } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import * as $ from 'jquery';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  private isCollapsed = false;

  private darkNavBar:boolean;

  private currentURL:string;

  constructor(router:Router) { 

    router.events.forEach((event) => {
      if(event instanceof NavigationEnd) {

        this.currentURL = router.url;
        
        if (router.url === '/'  || router.url === '/memories') {
          this.darkNavBar = false;
        }
        else {
          this.darkNavBar = true;
        }

        if (router.url !== '/') {
          $('html, body').animate({
            scrollTop: 0
          });
        }
      }
    });
  }

  @HostListener('window:scroll') windowScroll() {
    if (this.currentURL === '/') {
      let number = document.body.scrollTop;
      if (number > 300) {
        this.darkNavBar = true;
      }
      else {
        this.darkNavBar = false;
      }
    }  
  }
}


