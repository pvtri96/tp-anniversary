import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { MemoriesComponent } from './memories/memories.component';
import { MemoriesBodyComponent } from './memories/body/body.component';
import { MilestonesComponent } from './milestones/milestones.component';
import { MileStoneSidebarComponent } from './milestones/sidebar/sidebar.component';
import { MileStoneBodyComponent } from './milestones/body/body.component';
import { HomeComponent } from './home/home.component';
import { HomeBodyComponent } from './home/body/body.component';
import { MainWrapperComponent } from './home/body/main-wrapper/main-wrapper.component';

const appRoutes: Routes = [
  { 
    path: '', 
    component: HomeComponent 
  },
  { 
    path: 'memories',      
    component: MemoriesComponent 
  },
  { 
    path: 'milestones',      
    component: MilestonesComponent 
  },
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeBodyComponent,
    MemoriesBodyComponent,
    FooterComponent,
    MainWrapperComponent,
    MileStoneBodyComponent,
    MileStoneSidebarComponent,
    MilestonesComponent,
    MemoriesComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    NgbModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
