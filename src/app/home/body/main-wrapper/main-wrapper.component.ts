import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'main-wrapper',
  templateUrl: './main-wrapper.component.html',
  styleUrls: ['./main-wrapper.component.scss']
})
export class MainWrapperComponent implements OnInit {

  private slideshowIMG = [
      'assets/image/header.jpg',
  ];

  private dateFormat = "YYYYMMDDHHmmss";
  private years;
  private months;
  private weeks;
  private days;
  private hours;
  private minutes;
  private seconds;

  constructor() { }
  
  ngOnInit() {

    this.updateTimer();

    setInterval(() => {
      this.updateTimer();
    }, 1000)
    
  }

  private updateTimer() {
    let now = moment();
    let from = moment(`20160214180000`, this.dateFormat);

    this.years = now.diff(from, 'years');
    
    from = moment(`${now.format('YYYY')}0214180000`, this.dateFormat);
    this.months = now.diff(from, 'months');

    let month = parseInt(now.format('MM')) - 1;
    let monthStr = month < 10 ? '0' + month : month;
    from = moment(`${now.format('YYYY')}${ monthStr }14180000`, this.dateFormat);
    this.days = now.diff(from, 'days');

    from = moment(`${now.format('YYYY')}${ now.format('MM') }${ parseInt(now.format('DD')) - 1 }180000`, this.dateFormat);
    this.hours = now.diff(from, 'hours');

    from = moment(`${now.format('YYYY')}${now.format('MM')}${now.format('DD')}${now.format('HH')}0000`, this.dateFormat);
    this.minutes = now.diff(from, 'minutes');
    
    from = moment(`${now.format('YYYY')}${now.format('MM')}${now.format('DD')}${now.format('HH')}${now.format('mm')}00`, this.dateFormat);
    this.seconds = now.diff(from, 'seconds');
  }
}
