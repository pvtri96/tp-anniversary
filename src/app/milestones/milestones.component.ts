import { Component, OnInit } from '@angular/core';
import { MileStoneBodyComponent } from './body/body.component';
import { MileStoneSidebarComponent } from './sidebar/sidebar.component';

@Component({
  selector: 'milestones',
  templateUrl: './milestones.component.html',
  styleUrls: ['./milestones.component.scss']
})
export class MilestonesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
