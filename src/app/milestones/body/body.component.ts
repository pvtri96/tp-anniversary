import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'milestone-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.scss']
})
export class MileStoneBodyComponent implements OnInit {

  dateInputFormat = 'DDMMYYYY';
  dateOutputFormat = 'DD/MM/YYYY';
  articles = [];
  lineHeight = 0;

  constructor() { 
    this.articles = [
      {
        title: 'The first day of our love',
        content: 'This is the day that we expressed our feeling about each other via messenger.',
        position: '0',
        date: moment('07022016', this.dateInputFormat).format(this.dateOutputFormat),
      },
      {
        title: 'Valentine',
        content: 'This is the day that we went to Lotte cinema to watch Tet\'s movie after Tet holiday.',
        position: '60',
        date: moment('14022016', this.dateInputFormat).format(this.dateOutputFormat),
      },
      {
        title: 'Our first dating',
        content: 'This is the day that we had a real dating after 3 weeks. This day also is the first time we hold their hand.',
        position: '120',
        date: moment('20022016', this.dateInputFormat).format(this.dateOutputFormat),
      },
      {
        title: 'The international Women day',
        content: 'I bought a heart shape card in Hoi An and present to her.',
        position: '250',
        date: moment('08032016', this.dateInputFormat).format(this.dateOutputFormat),
      },
      {
        title: '1 month anniversary',
        content: 'In this day, We went to Bac My An market to eat Kem Bo ^__^ Then, she gifts me a special hand-made present. This is my first time I receive a gift from a girl friend.',
        position: '310',
        date: moment('14032016', this.dateInputFormat).format(this.dateOutputFormat),
      },
      {
        title: 'Someday in April',
        content: 'I said "I love you" to her for the first time in the movie theater. She expected me to say to her earlier but with me it\'s still very hard to say that.',
        position: '500',
        date: moment('04042016', this.dateInputFormat).format(this.dateOutputFormat),
      },
      {
        title: 'Our first kiss <3',
        content: 'We have the first kiss, it\'s wonderful. This is also our first kiss.',
        position: '560',
        date: moment('14042016', this.dateInputFormat).format(this.dateOutputFormat),
      },
      {
        title: 'First time to her home',
        content: 'After 2 dating month, she invited me to come to her home. I felt scared till when I met her parent, they are very friendly so that make me forget the scared feeling. Then, we go to Lotte and watch a movies.',
        position: '680',
        date: moment('28042016', this.dateInputFormat).format(this.dateOutputFormat),
      },
      {
        title: 'Cua Dai beach, Hoi An',
        content: 'In this day, she had a feast with her friends start from 10:00 AM but she left them at 01:00 PM and told me she want to go to Hoi An. Of course, I said yes and we start to go at 01:00 PM. This\'s our first time travel far from Da Nang and we have a lot of memories in here.',
        position: '900',
        date: moment('24072016', this.dateInputFormat).format(this.dateOutputFormat),
      },
      {
        title: 'My birthday',
        content: 'In my birthday, I took her to Than Tai mountain which is a swimming pool was build near Ba Na mountain. We played in there from morning till afternoon and spent almost time in the pool because she love swimming pool a lot. After that, she gift me a 6 angles card which she make by herself. In the evening, we went to our secret place and stayed there till 09:00 PM ^__^',
        position: '1020',
        date: moment('02092016', this.dateInputFormat).format(this.dateOutputFormat),
      },
      {
        title: 'Her birthday',
        content: 'In this day, I came to her home at 04:00 PM to find a birthday cake. We came to BonPas bakery but there is\'nt any good cake for us. So that we decided to go for eating first, we went to Motte Pizza. After that, we went to a bakery near that and find a nice cake for her. Then we went to a So Yummy Icecream and take some selfie with the cake. Finally, we went to our secret place and stayed there till 09:00 PM.',
        position: '1230',
        date: moment('18112016', this.dateInputFormat).format(this.dateOutputFormat),
      },
      {
        title: 'Our first Christmas together',
        content: 'We go to Vincom together and take a selfie.',
        position: '1360',
        date: moment('24122016', this.dateInputFormat).format(this.dateOutputFormat),
      },
      {
        title: 'Our 1 year anniversary',
        content: 'We booked a special cake which have html code on this. Then we went to Motte pizza to eat and waiting for the cake, it\'s really taken a lot of time to wait for the shipper but we still had this at 06:00 PM. After that, we go to our secret place till 09:00 PM.',
        position: '1600',
        date: moment('14022017', this.dateInputFormat).format(this.dateOutputFormat),
      },
    ];

    this.lineHeight = 1800;
  }

  ngOnInit() {
  }


}
