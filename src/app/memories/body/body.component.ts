import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'memories-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.scss']
})

export class MemoriesBodyComponent implements OnInit {

  private slides: Array<Object>;

  constructor() {
    this.slides = [
      {
        title: 'Cua Dai beach, Hoi An',
        content: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis, aut, harum. Veritatis sunt labore exercitationem rem hic perspiciatis totam, eveniet similique perferendis ea facere quos inventore quasi vel officiis libero.`,
        image: 'cuadai.jpg',
      },
      {
        title: 'Than Tai mountain, Da Nang',
        content: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias ducimus error eius, aut expedita reprehenderit quas, facilis maxime aspernatur dolores quidem harum optio culpa nobis accusamus suscipit ab! Eos, excepturi.`,
        image: 'thantai.jpg',
      },
      {
        title: 'Vincom, Da Nang',
        content: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias ducimus error eius, aut expedita reprehenderit quas, facilis maxime aspernatur dolores quidem harum optio culpa nobis accusamus suscipit ab! Eos, excepturi.`,
        image: 'vincom.jpg',
      },
      {
        title: 'Mote pizza, Da Nang',
        content: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias ducimus error eius, aut expedita reprehenderit quas, facilis maxime aspernatur.`,
        image: 'mote.jpg',
      },
    ];
   }

  ngOnInit() {
  }

}
