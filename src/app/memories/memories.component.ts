import { Component, OnInit } from '@angular/core';
import { MemoriesBodyComponent } from './body/body.component';

@Component({
  selector: 'memories',
  templateUrl: './memories.component.html',
  styleUrls: ['./memories.component.scss']
})
export class MemoriesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
